import React from 'react';
import { TextInput, StyleSheet } from 'react-native';

const TextNote = (props) => {
  const [text, onChangeText] = React.useState(props.defaultText);

  return (
      <TextInput 
      placeholder={props.placeholder}
      style={styles.input}
      onChangeText={onChangeText}
      multiline={true}
      value={text}/>
  );
};

const styles = StyleSheet.create({
  input: {
    height: '100%',
    padding: 10,
    width: '100%',
    textAlignVertical: 'top'
  },
});



export default TextNote;