import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { IconButton } from 'react-native-paper';
import Home from '../screens/SelectNote'
import Note from '../screens/Note'

const Stack = createNativeStackNavigator();

const Routes = () => {
   return (
      <NavigationContainer>
         <Stack.Navigator>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Note" component={Note} />
         </Stack.Navigator>
      </NavigationContainer>
   );
}

export default Routes