import React from 'react';
import Routes from './navigation/routes';
import { PaperProvider } from 'react-native-paper';


function App() {
  return (
    <PaperProvider>
      <Routes />
    </PaperProvider>
  )
}

export default App;