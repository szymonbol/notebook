import React from 'react'
import { IconButton, Button, Modal, Portal, Text, PaperProvider } from 'react-native-paper';
import { StyleSheet, View } from 'react-native';
import TextNote from '../components/TextNote'

const styles = StyleSheet.create({
   leftButton: {
      marginRight: 20,
      marginLeft: -10,
   },
   containerStyle: {
      backgroundColor: 'white',
      padding: 20,
      height: 140,
      marginHorizontal: 30
   },
   modalButtons: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: 40
   },
   yesNoButtons: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'flex-end'
   },
   saveButtons: {
      width: 80,
      marginLeft: 5
   }
});

const Note = ({ navigation }) => {
   const [visible, setVisible] = React.useState(false);

   const showModal = () => setVisible(true);
   const hideModal = () => setVisible(false);

   React.useEffect(() => {
      navigation.setOptions({
         headerLeft: () => (
            <IconButton style={styles.leftButton} icon="arrow-left" onPress={() => showModal()} />
         ),
         headerRight: () => (
            <IconButton icon="check" onPress={() => saveNote()} />
         ),
      });
   }, [navigation]);



   const DiscardOrSaveModal = () => {
      return (
         <Portal>
            <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={styles.containerStyle}>
               <Text> Czy chcesz zapisać zmiany przed wyjściem? </Text>
               <View style={styles.modalButtons}>
                  <Button onPress={() => { hideModal() }}>Anuluj</Button>
                  <View style={styles.yesNoButtons}>
                     <Button style={styles.saveButtons} mode="elevated" onPress={() => { navigation.goBack() }}>Nie</Button>
                     <Button style={styles.saveButtons} mode="contained" onPress={() => { saveNote() }}>Tak</Button>
                  </View>
               </View>
            </Modal>
         </Portal>
      )
   }

   function saveNote() {
      alert('Trying to save... but its not implemented yet');
      navigation.goBack()
   }

   return (
      <View >
         <DiscardOrSaveModal />
         <TextNote />
      </View >
   )
}
export default Note